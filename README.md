# PseudoShell
Pseudonymisierungsdienst in Bash mit OpenPGP Encryption

Das Script implementiert drei Rollen: Sender, Intermediate, Receiver.

## Sender
Die Nutzdaten werden einzeln pro Datensatz für den Receiver verschlüsselt. Die identifizierenden Daten werden mit den zugeordneten und verschlüsselten Nutzdaten gemeinsam für den Intermediate verschlüsselt. Die Übertragung erfolgt per OpenBSD NetCat an den Intermediate.

## Intermediate
Der Intermediate entschlüsselt das Paket und tauscht die identifizierenden Daten gegen zufallsgenerierte Pseudonyme aus. Die Zuordnung der identifizierenden Daten zu den Pseudonymen wird in der Datei `assignments` abgelegt. Die Datei wird auch als Nachschlagewerk genutzt, sodass gleichen identifizierenden Daten jeweils das gleiche Pseudonym zugeordnet wird. Das neue Paket aus Pseudonymen und vercshlüsselten Nutzdaten wird für den Receiver verschlüsselt.

## Receiver
Der Receiver entschlüsselt das Paket und anschließend zeilenweise die Nutzdaten. Das Ergebnis wird in eine Datei mit Zeitstempel als Dateinamen gespeichert.

