#!/bin/bash
function sender() {
	local SEPARATOR=","
	local INPUT_FILE="source.csv"
	local IDAT_NAME="IDAT"
	local SENDTO="localhost:10042"
	local INTERMEDIATEKEY="Intermediate"
	local RECEIVERKEY="Receiver"

	TEMP=`getopt -o hs:f:i:t:n:r: --long help,separator:,file:,idat:,send-to:,intermediate-key:,receiver-key: -n "${CMD} --${FUNCNAME}" -- "${@}"`

	if [ $? != 0 ] ; then return 1 ; fi

	eval set -- "${TEMP}";

	while [[ ${1:0:1} = - ]]; do
		case $1 in
			-h|--help)
				cat <<EOF
USAGE: ${CMD} --sender [OPTIONS]

OPTIONS

  -h --help		Print this help
  -s --separator	Set a character as field separator 	(default: "${SEPARATOR}")
  -f --file		Set a character as field separator 	(default: "${INPUT_FILE}")
  -i --idat		Set a character as field separator 	(default: "${IDAT_NAME}")
  -t --send-to		Send output to 				(default: ${SENDTO})
  -n --intermediate-key	UID of the public-key for intermediate 	(default: "${INTERMEDIATEKEY}")
  -r --receiver-key	UID of the public-key for receiver 	(default: "${RECEIVERKEY}")

EOF
											shift 1; return ;;
			--)								shift 1; break ;;
			-s|--separator)		local SEPARATOR="${2}";			shift 2; continue ;;
			-f|--file)		local INPUT_FILE="${2}";			shift 2; continue ;;
			-i|--idat)		local IDAT_NAME="${2}";			shift 2; continue ;;
			-t|--send-to)		local SENDTO="${2}";			shift 2; continue ;;
			-n|--intermediate-key)	local INTERMEDIATEKEY="${2}";		shift 2; continue ;;
			-r|--receiver-key)	local RECEIVERKEY="${2}";		shift 2; continue ;;
		esac

		echo "ERROR: Unknown parameter ${1}"
		break;
	done

	INPUT=`cat "${INPUT_FILE}"`
	IDAT_INDEX=$(echo "${INPUT}" | head -1 - | tr "$SEPARATOR" '\n' | nl | grep -w "$IDAT_NAME" | tr -d " " | awk -F " " '{print $1}')
	IDAT=$(echo "$INPUT" | cut -d "$SEPARATOR" -f$IDAT_INDEX -)
	MDAT=$(echo "$INPUT" | cut -d "$SEPARATOR" --complement -f$IDAT_INDEX -)
	MDAT_ENC=""
	while read -r LINE; do
		if [[ ! -z "$MDAT_ENC" ]]; then
			MDAT_ENC+=$'\n'
		fi
		MDAT_ENC+="$(echo "$LINE" | gpg2 --encrypt --recipient "Receiver" | base64 -w0)"
	done < <(echo "$MDAT")
	OUTPUT=$(paste --delimiter="$SEPARATOR" <(echo "$IDAT") <(echo "$MDAT_ENC"))
	OUTPUT_ENC=$(echo "$OUTPUT" | gpg2 --encrypt --recipient "Intermediate" | base64 -w0)
	echo "$OUTPUT_ENC" | nc -N $(echo $SENDTO | cut -d ":" -f1) $(echo $SENDTO | cut -d ":" -f2)
}

function intermediate() {
	local SEPARATOR=","
	local LISTEN="10042"
	local SENDTO="localhost:10043"
	local MAXPSN="999999999"
	local RECEIVERKEY="Receiver"

	TEMP=`getopt -o hs:l:t:m:r: --long help,separator:,listen:,send-to:,max-psn:,receiver-key: -n "${CMD} --${FUNCNAME}" -- "${@}"`

	if [ $? != 0 ] ; then return 1 ; fi

	eval set -- "${TEMP}";

	while [[ ${1:0:1} = - ]]; do
		case $1 in
			-h|--help)
				cat <<EOF
USAGE: ${CMD} --intermediate [OPTIONS]

OPTIONS

  -h --help		Print this help
  -s --separator	Set a character as field separator 	(default: "${SEPARATOR}")
  -l --listen		Listen to PORT for input 		(default: ${LISTEN})
  -t --send-to		Send output to 				(default: ${SENDTO})
  -m --max-psn		Upper limit of PSN random numbers 	(default: ${MAXPSN})
  -r --receiver-key	UID of the public-key for receiver 	(default: "${RECEIVERKEY}")

EOF
											shift 1; return ;;
			--)								shift 1; break ;;
			-s|--separator)		local SEPARATOR="${2}";			shift 2; continue ;;
			-l|--listen)		local LISTEN="${2}";			shift 2; continue ;;
			-t|--send-to)		local SENDTO="${2}";			shift 2; continue ;;
			-m|--max-psn)		local MAXPSN="${2}";			shift 2; continue ;;
			-r|--receiver-key)	local RECEIVERKEY="${2}";		shift 2; continue ;;
		esac

		echo "ERROR: Unknown parameter ${1}"
		break;
	done

	while true; do
		INPUT=$(nc -l $LISTEN | base64 -d | gpg2 --decrypt 2>/dev/null)
		IDAT=$(echo "$INPUT" | cut -d "$SEPARATOR" -f1 -)
		MDAT_ENC=$(echo "$INPUT" | cut -d "$SEPARATOR" --complement -f1 -)
		PSN="PSN"
		while read -r _IDAT; do
			PSN+=$'\n'
			# START TRANSACTION!!!
			if [[ ! -f assignments ]]; then
				touch assignments
			fi
			_ASSIGNMENT=$(grep "^${_IDAT}," assignments)
			if [ $? -eq 0 ]; then
				_PSN="$(echo "${_ASSIGNMENT}" | cut -d "," -f2 -)"
			else
				_GEN=0
				while [ ${_GEN} -eq 0 ]; do
					_PSN=$(shuf -i1-$MAXPSN -n1)
					grep -q ",${_PSN}$" assignments
					_GEN=$?
				done
				echo "${_IDAT},${_PSN}" >> assignments
			fi
			# END TRANSACTION!!!
			PSN+="${_PSN}"
		done < <(echo "$IDAT" | tail -n +2)
		OUTPUT=$(paste --delimiter="$SEPARATOR" <(echo "$PSN") <(echo "$MDAT_ENC"))
		OUTPUT_ENC=$(echo "$OUTPUT" | gpg2 --encrypt --recipient "$RECEIVERKEY" | base64 -w0)
		echo "$OUTPUT_ENC" | nc -N $(echo $SENDTO | cut -d ":" -f1) $(echo $SENDTO | cut -d ":" -f2)
	done
}

function receiver() {
	local SEPARATOR=","
	local LISTEN="10043"

	TEMP=`getopt -o hs:l: --long help,separator:,listen: -n "${CMD} --${FUNCNAME}" -- "${@}"`

	if [ $? != 0 ] ; then return 1 ; fi

	eval set -- "${TEMP}";

	while [[ ${1:0:1} = - ]]; do
		case $1 in
			-h|--help)
				cat <<EOF
USAGE: ${CMD} --receiver [OPTIONS]

OPTIONS

  -h --help		Print this help
  -s --separator	Set a character as field separator 	(default: "${SEPARATOR}")
  -l --listen		Listen to PORT for input 		(default: ${LISTEN})

EOF
											shift 1; return ;;
			--)								shift 1; break ;;
			-s|--separator)		local SEPARATOR="${2}";			shift 2; continue ;;
			-l|--listen)		local LISTEN="${2}";			shift 2; continue ;;
		esac

		echo "ERROR: Unknown parameter ${1}"
		break;
	done

	while true; do
		INPUT=$(nc -l $LISTEN)
		DEC1=$(echo "$INPUT" | base64 -d | gpg2 --decrypt 2>/dev/null)
		PSN=$(echo "$DEC1" | cut -d "," -f1 -)
		MDAT_ENC=$(echo "$DEC1" | cut -d "," --complement -f1 -)
		MDAT=""
		while read -r LINE; do
			if [[ ! -z "$MDAT" ]]; then
				MDAT+=$'\n'
			fi
			MDAT+="$(echo "$LINE" | base64 -d | gpg2 --decrypt 2>/dev/null)"
		done < <(echo "$MDAT_ENC")
		OUTPUT=$(paste --delimiter="$SEPARATOR" <(echo "$PSN") <(echo "$MDAT"))
		echo "$OUTPUT" > "./$(date "+%Y-%m-%d_%H-%M-%S")"
	done
}

CMD=`basename ${0}`

case $1 in
	-h|--help)
		cat <<EOF
USAGE: ${CMD} [MODE] [OPTIONS]

MODE

  -h --help		Print this help

  -S --sender		Start as sender
  -I --intermediate	Start as intermediate
  -R --receiver		Start as receiver
  
OPTIONS

  For OPTIONS use
    ${CMD} [MODE] -h | --help

EOF
      			shift 1; exit 0 ;;
    --)			shift 1; exit 0 ;;
    -S|--sender)	shift 1; sender "${@}"; exit $?; ;;
    -I|--intermediate)	shift 1; intermediate "${@}"; exit $?; ;;
    -R|--receiver)	shift 1; receiver "${@}"; exit $?; ;;
  esac

  echo "ERROR: Unknown parameter ${1}"
done

